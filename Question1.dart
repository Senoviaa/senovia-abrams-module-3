Column(
  mainAxisSize: MainAxisSize.max,
  children: [
    Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
        color: Color(0xFFEEEEEE),
      ),
      child: Image.network(
        'https://picsum.photos/seed/712/600',
        width: 100,
        height: 100,
        fit: BoxFit.cover,
      ),
    ),
    TextFormField(
      controller: textController1,
      onChanged: (_) => EasyDebounce.debounce(
        'textController1',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'usename',
        hintText: 'usename',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    Divider(),
    Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
        color: Color(0xFFEEEEEE),
      ),
      child: TextFormField(
        controller: textController2,
        onChanged: (_) => EasyDebounce.debounce(
          'textController2',
          Duration(milliseconds: 2000),
          () => setState(() {}),
        ),
        autofocus: true,
        obscureText: !passwordVisibility,
        decoration: InputDecoration(
          labelText: 'password',
          hintText: 'password',
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0x00000000),
              width: 1,
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4.0),
              topRight: Radius.circular(4.0),
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0x00000000),
              width: 1,
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4.0),
              topRight: Radius.circular(4.0),
            ),
          ),
          suffixIcon: InkWell(
            onTap: () => setState(
              () => passwordVisibility = !passwordVisibility,
            ),
            focusNode: FocusNode(skipTraversal: true),
            child: Icon(
              passwordVisibility
                  ? Icons.visibility_outlined
                  : Icons.visibility_off_outlined,
              color: Color(0xFF757575),
              size: 22,
            ),
          ),
        ),
        style: FlutterFlowTheme.of(context).bodyText1,
      ),
    ),
    Divider(),
    FFButtonWidget(
      onPressed: () {
        print('Button pressed ...');
      },
      text: 'login',
      options: FFButtonOptions(
        width: 130,
        height: 40,
        color: FlutterFlowTheme.of(context).primaryColor,
        textStyle: FlutterFlowTheme.of(context).subtitle2.override(
              fontFamily: 'Poppins',
              color: Colors.white,
            ),
        borderSide: BorderSide(
          color: Colors.transparent,
          width: 1,
        ),
        borderRadius: 12,
      ),
    ),
    Divider(),
    FFButtonWidget(
      onPressed: () {
        print('Button pressed ...');
      },
      text: 'registration',
      options: FFButtonOptions(
        width: 130,
        height: 40,
        color: FlutterFlowTheme.of(context).primaryColor,
        textStyle: FlutterFlowTheme.of(context).subtitle2.override(
              fontFamily: 'Poppins',
              color: Colors.white,
            ),
        borderSide: BorderSide(
          color: Colors.transparent,
          width: 1,
        ),
        borderRadius: 12,
      ),
    ),
  ],
)

