Scaffold(
  key: scaffoldKey,
  appBar: AppBar(
    backgroundColor: FlutterFlowTheme.of(context).primaryColor,
    automaticallyImplyLeading: true,
    title: Text(
      'Feature 2',
      style: FlutterFlowTheme.of(context).title2.override(
            fontFamily: 'Poppins',
            color: Colors.white,
            fontSize: 22,
          ),
    ),
    actions: [],
    centerTitle: false,
    elevation: 2,
  ),
  backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
  body: SafeArea(
    child: GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              color: Color(0xFFEEEEEE),
            ),
            child: Image.network(
              'https://picsum.photos/seed/886/600',
              width: 100,
              height: 100,
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    ),
  ),
)

