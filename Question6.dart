Column(
  mainAxisSize: MainAxisSize.max,
  children: [
    Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        color: Color(0xFFEEEEEE),
      ),
      child: TextFormField(
        controller: textController1,
        onChanged: (_) => EasyDebounce.debounce(
          'textController1',
          Duration(milliseconds: 2000),
          () => setState(() {}),
        ),
        autofocus: true,
        obscureText: false,
        decoration: InputDecoration(
          labelText: 'username',
          hintText: 'username',
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0x00000000),
              width: 1,
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4.0),
              topRight: Radius.circular(4.0),
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0x00000000),
              width: 1,
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4.0),
              topRight: Radius.circular(4.0),
            ),
          ),
        ),
        style: FlutterFlowTheme.of(context).bodyText1,
      ),
    ),
    Divider(),
    TextFormField(
      controller: textController2,
      onChanged: (_) => EasyDebounce.debounce(
        'textController2',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'profile',
        hintText: 'profile',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    Divider(),
    TextFormField(
      controller: textController3,
      onChanged: (_) => EasyDebounce.debounce(
        'textController3',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'password',
        hintText: 'password',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    Divider(),
    TextFormField(
      controller: textController4,
      onChanged: (_) => EasyDebounce.debounce(
        'textController4',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'Email',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    Divider(),
    TextFormField(
      controller: textController5,
      onChanged: (_) => EasyDebounce.debounce(
        'textController5',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'gender',
        hintText: 'gender',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    Divider(),
    FFButtonWidget(
      onPressed: () {
        print('Button pressed ...');
      },
      text: 'update',
      options: FFButtonOptions(
        width: 130,
        height: 40,
        color: FlutterFlowTheme.of(context).primaryColor,
        textStyle: FlutterFlowTheme.of(context).subtitle2.override(
              fontFamily: 'Poppins',
              color: Colors.white,
            ),
        borderSide: BorderSide(
          color: Colors.transparent,
          width: 1,
        ),
        borderRadius: 12,
      ),
    ),
  ],
)

